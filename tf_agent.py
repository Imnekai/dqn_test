import tensorflow as tf
import numpy as np
from tensorflow.contrib.rnn.python.ops import core_rnn_cell_impl
from memory import Memory
from collections import namedtuple
import sys
import utils

linear = core_rnn_cell_impl._linear 

Transition = namedtuple('Transition', ('state','action','next_state','reward','mask'))

def huber_loss(y_true, y_pred):
    err = tf.abs(y_true - y_pred, name='abs')
    lin = 2*err-1
    quad = tf.square(y_true-y_pred)
    return tf.where(err < 1, quad, lin)

class TFDQN(object):

	def __init__(self, input_len=4, output_len=2, gamma = 0.9):
		self._input_len = input_len
		self._output_len = output_len
		self._gamma = gamma
		self._session = tf.Session()
		self._memory = Memory(5000)
		self._create_graph()


	def forward(self,x, reuse = True):
			self._lin1 = tf.tanh(utils.lin(x, 40, 'lin1', reuse))
			self._lin2 = tf.tanh(utils.lin(self._lin1, 40, 'lin2', reuse))
			output = utils.lin(self._lin2, self._output_len, 'output', reuse)
			return output

	def _create_graph(self):

		self._input = tf.placeholder(tf.float32, [None, self._input_len])
		self._output = self.forward(self._input, reuse = False)
		

		self._actions = tf.placeholder_with_default(tf.zeros([1,1], dtype=tf.int32), [None, 1])
		self._next_states = tf.placeholder_with_default(tf.zeros([1,self._input_len]), [None, self._input_len])
		self._rewards = tf.placeholder_with_default(tf.zeros([1]), [None])
		self._mask = tf.placeholder_with_default(tf.zeros([1]), [None])

		self._estimate_q = tf.stop_gradient(self._gamma*tf.reduce_max(self.forward(self._next_states), axis=1)*self._mask+self._rewards)

		self.select = tf.expand_dims(tf.range(0,tf.shape(self._mask)[0],dtype = tf.int32),-1)
		self.select = tf.concat([self.select,self._actions], axis=1)

		self._action_values = tf.gather_nd(self._output,self.select)

		self._loss = tf.reduce_mean(huber_loss(self._estimate_q,self._action_values))

	def _take_action(self, state, epsilon=0):
		if np.random.uniform()>epsilon:
			vals = self._session.run(self._output, feed_dict={self._input: [state]})
			return np.argmax(vals)
		else:
			return (int)(np.random.uniform(0,2))


		

	def train(self, env, episodes, batch_size = 100, learning_rate = 1e-3, eps_min=0.05, eps_max=0.9,eps_decay=2000.0):
		self._optimizer = tf.train.AdamOptimizer(learning_rate)
		self._optimize_step = self._optimizer.minimize(self._loss)
		self._session.run(tf.global_variables_initializer())
		total_steps = 0
		for episode in range(episodes):
			env.reset()
			done = False
			steps = 0
			total_loss = 0.0
			prev_state = env.state
			while not done and steps<1000:
				env.render()
				epsilon = eps_min + (eps_max-eps_min)/np.exp(total_steps/eps_decay)
				action = self._take_action(prev_state,epsilon)
				next_state, reward, done, info = env.step(action)
				if not done:
					self._memory.add(Transition(prev_state,action,next_state,reward,1))
				else:
					self._memory.add(Transition(prev_state,action,next_state,reward,0))
				prev_state = next_state
				if len(self._memory)>=batch_size:
					sample = self._memory.sample(batch_size)
					sample = Transition(*zip(*sample))
					_, loss, estimate_q, action_values, mask, select, output = self._session.run([self._optimize_step, self._loss, self._estimate_q, self._action_values, self._mask, self.select, self._output], feed_dict = {self._input: sample.state, self._actions: np.expand_dims(sample.action,-1), 
						self._next_states: sample.next_state, self._rewards: sample.reward, self._mask: sample.mask})
					total_loss += loss
				steps+=1
			total_steps+=steps
			print('Episode[%d] Steps[%d] Loss[%.4f]'%(episode,steps, total_loss/steps))
			sys.stdout.flush()



class TFDDQN(object):

	def __init__(self, input_len=4, output_len=2, gamma = 0.9):
		self._input_len = input_len
		self._output_len = output_len
		self._gamma = gamma
		self._session = tf.Session()
		self._memory = [Memory(5000),Memory(5000)]
		self._create_graph()


	def forward(self,x, nr, reuse = True):
			with tf.variable_scope('q_network_%d'%nr):
				self._lin1 = tf.tanh(utils.lin(x, 40, 'lin1', reuse))
				self._lin2 = tf.tanh(utils.lin(self._lin1, 40, 'lin2', reuse))
				output = utils.lin(self._lin2, self._output_len, 'output', reuse)
				return output

	def _create_graph(self):

		self._input = [tf.placeholder_with_default(tf.zeros([1,self._input_len]), [None, self._input_len]),
										tf.placeholder_with_default(tf.zeros([1,self._input_len]), [None, self._input_len])]
		self._output = []
		for i in range(2):
			self._output.append(self.forward(self._input[i], i, reuse = False))
		

		self._actions = [tf.placeholder_with_default(tf.zeros([1,1], dtype=tf.int32), [None, 1]),tf.placeholder_with_default(tf.zeros([1,1], dtype=tf.int32), [None, 1])]
		self._next_states = [tf.placeholder_with_default(tf.zeros([1,self._input_len]), [None, self._input_len]),tf.placeholder_with_default(tf.zeros([1,self._input_len]), [None, self._input_len])]
		self._rewards = [tf.placeholder_with_default(tf.zeros([1]), [None]),tf.placeholder_with_default(tf.zeros([1]), [None])]
		self._mask = [tf.placeholder_with_default(tf.zeros([1]), [None]),tf.placeholder_with_default(tf.zeros([1]), [None])]

		self._estimate_q = []
		self._loss = []
		self._action_values = []
		for i in range(2):

			other = (i+1)%2
			actions = tf.expand_dims(tf.cast(tf.argmax(self.forward(self._next_states[i],i), axis=1), tf.int32),-1)
			self.select = tf.expand_dims(tf.range(0,tf.shape(self._mask[i])[0],dtype = tf.int32),-1)
			self.select = tf.concat([self.select,actions], axis=1)
			self._estimate_q.append(tf.stop_gradient(self._gamma*tf.gather_nd(self.forward(self._next_states[i],other),self.select))*self._mask[i]+self._rewards[i])

			self.select = tf.expand_dims(tf.range(0,tf.shape(self._mask[i])[0],dtype = tf.int32),-1)
			self.select = tf.concat([self.select,self._actions[i]], axis=1)

			self._action_values.append(tf.gather_nd(self._output[i],self.select))

			self._loss.append(tf.reduce_mean(huber_loss(self._estimate_q[i],self._action_values[i])))
		self._total_loss = self._loss[0]+self._loss[1]

	def _take_action(self, state, epsilon=0):
		if np.random.uniform()>epsilon:
			nr = int(np.random.sample()<0.5)
			vals = self._session.run(self._output[nr], feed_dict={self._input[nr]: [state]})
			return np.argmax(vals)
		else:
			return (int)(np.random.uniform(0,2))


		

	def train(self, env, episodes, batch_size = 100, learning_rate = 1e-3, eps_min=0.05, eps_max=0.9,eps_decay=2000.0):
		self._optimizer = tf.train.AdamOptimizer(learning_rate)
		self._optimize_step = self._optimizer.minimize(self._total_loss)
		self._session.run(tf.global_variables_initializer())
		total_steps = 0
		for episode in range(episodes):
			nr = episode%2
			env.reset()
			done = False
			steps = 0
			total_loss = 0.0
			prev_state = env.state
			while not done and steps<1000:
				env.render()
				epsilon = eps_min + (eps_max-eps_min)/np.exp(total_steps/eps_decay)
				action = self._take_action(prev_state,epsilon)
				next_state, reward, done, info = env.step(action)
				if not done:
					self._memory[nr].add(Transition(prev_state,action,next_state,reward,1))
				else:
					self._memory[nr].add(Transition(prev_state,action,next_state,reward,0))
				prev_state = next_state
				if len(self._memory[0])>=batch_size and len(self._memory[1])>=batch_size:
					sample_0 = self._memory[0].sample(batch_size)
					sample_0 = Transition(*zip(*sample_0))
					sample_1 = self._memory[1].sample(batch_size)
					sample_1 = Transition(*zip(*sample_1))
					_, loss = self._session.run([self._optimize_step, self._total_loss], feed_dict = {self._input[0]: sample_0.state, self._input[1]:sample_1.state, self._actions[0]: np.expand_dims(sample_0.action,-1), self._actions[1]: np.expand_dims(sample_1.action,-1), 
						self._next_states[0]: sample_0.next_state, self._next_states[1]:sample_1.next_state, self._rewards[0]: sample_0.reward, self._rewards[1]: sample_1.reward, self._mask[0]: sample_0.mask, self._mask[1]: sample_1.mask})
					total_loss += loss
				steps+=1
			total_steps+=steps
			print('Episode[%d] Steps[%d] Loss[%.4f]'%(episode,steps, total_loss/steps))
			sys.stdout.flush()