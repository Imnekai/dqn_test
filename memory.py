import random

class Memory(object):

	def __init__(self, capacity):
		self.mem = []
		self.capacity = capacity
		self.pos = 0

	def add(self,transition):
		if len(self.mem)<=self.pos:
			self.mem.append(None)
		self.mem[self.pos] = transition
		self.pos = (self.pos+1)%self.capacity

	def sample(self, size):
		return random.sample(self.mem, size)


	def __len__(self):
		return len(self.mem)