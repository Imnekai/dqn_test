import torch
import torch.nn.functional as F

class QEstimator(torch.nn.Module):

	def __init__(self, state_size = 4, action_space_size = 2):
		super(QEstimator, self).__init__()
		self.lin1 = torch.nn.Linear(state_size, 40)
		self.lin2 = torch.nn.Linear(40,20)
		self.lin3 = torch.nn.Linear(20,action_space_size)

	def forward(self,x):
		x = F.tanh(self.lin1(x))
		x = F.tanh(self.lin2(x))
		x = self.lin3(x)
		return x