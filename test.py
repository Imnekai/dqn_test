import tensorflow as tf
import numpy as np

a = tf.placeholder(tf.float32, [2,2])
s = tf.Session()

print(s.run(a[, feed_dict={a:[[3,2],[5,7]]}))