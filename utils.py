import tensorflow as tf


def lin(x, out, scope = 'lin', reuse = True):
	with tf.variable_scope(scope, reuse = reuse):
		w = tf.get_variable('w',[x.get_shape().as_list()[-1], out], tf.float32, initializer = tf.contrib.layers.xavier_initializer())
		b = tf.get_variable('b',[out], tf.float32)
		return tf.matmul(x,w)+b