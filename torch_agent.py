import torch
from q_estimator import QEstimator
from memory import Memory
import numpy as np
import sys
from collections import namedtuple
import torch.nn.functional as F

USE_CUDA = torch.cuda.is_available()

Transition = namedtuple('Transition',('state','action','next_state','reward'))

class Variable(torch.autograd.Variable):

	def __init__(self, x, *args, **kargs):
		if USE_CUDA:
			x = x.cuda()
		return super(Variable, self).__init__(x,*args,**kargs)

class TorchDQN(object):

	def __init__(self,memory_capacity=10000,state_size=4,action_space_size=2, gamma = 0.7):
		self.mem = Memory(memory_capacity)
		self.q = QEstimator(state_size,action_space_size)
		self.action_space_size = action_space_size
		self.state_size = state_size
		self.gamma = gamma


	def take_action(self, state, epsilon=0):
		self.total_steps+=1
		q_score = self.q(Variable(torch.Tensor(state).view(1,self.state_size),volatile=True)).data.numpy()
		print(q_score)
		if epsilon < np.random.sample():
			return (int)(np.argmax(q_score))
		else:
			return (int)(np.random.uniform(0,self.action_space_size))


	def train(self, env, episodes, batch_size = 100, eps_start = 0.9, eps_end = 0.05, eps_decay=2000):
		self.optimizer = torch.optim.Adam(self.q.parameters(), lr=0.001)
		self.total_steps = 0
		for episode in range(episodes):
			print('Episode [%d]'%episode)
			sys.stdout.flush()
			env.reset()
			done = False
			state = env.state
			steps = 0
			loss = 0.0
			
			while not done and steps < 2000:
				env.render()
				epsilon = eps_end + (eps_start-eps_end) * np.exp(-(self.total_steps)/eps_decay)
				action = self.take_action(state,epsilon)
				next_state, reward, done, info = env.step(action)
				if done:
					self.mem.add(Transition(torch.Tensor(state).unsqueeze(0),torch.LongTensor([action]).unsqueeze(0),
					None,torch.Tensor([reward]).unsqueeze(0)))
				else:
					self.mem.add(Transition(torch.Tensor(state).unsqueeze(0),torch.LongTensor([action]).unsqueeze(0),
					torch.Tensor(next_state).unsqueeze(0),torch.Tensor([reward]).unsqueeze(0)))
				
				state = next_state
				steps+=1
				loss += self._optimize(batch_size)
			print('Steps [%d] Loss[%.4f]'%(steps,loss/steps))

	def _optimize(self, batch_size):
		if len(self.mem)<batch_size:
			return 0
		batch = self.mem.sample(batch_size)
		batch = Transition(*zip(*batch))
		mask = []
		for state in batch.next_state:
			if state is None:
				mask.append(0)
			else:
			 mask.append(1)
		mask = torch.ByteTensor(mask)
		state_batch = Variable(torch.cat(batch.state))
		action_batch = Variable(torch.cat(batch.action))
		next_state_batch = Variable(torch.cat([state for state in batch.next_state if state is not None]),volatile = True)
		reward_batch = Variable(torch.cat(batch.reward))

		current_values = self.q(state_batch).gather(1,action_batch)
		next_values = Variable(torch.zeros(batch_size))
		next_values[mask] = self.q(next_state_batch).max(1)[0]
		next_values.volatile = False
		exp_next_values = reward_batch + self.gamma*next_values
		
		loss = F.smooth_l1_loss(current_values, exp_next_values)
		self.optimizer.zero_grad()
		loss.backward()
		self.optimizer.step()
		return loss.data.numpy()


class TorchDDQN(object):
	'''
	TO-DO: implement torch double dqn
	'''

	def __init__(self,memory_capacity=10000,state_size=4,action_space_size=2, gamma = 0.7):
		self.mem = Memory(memory_capacity)
		self.q = QEstimator(state_size,action_space_size)
		self.action_space_size = action_space_size
		self.state_size = state_size
		self.gamma = gamma


	def take_action(self, state, epsilon=0):
		self.total_steps+=1
		q_score = self.q(Variable(torch.Tensor(state).view(1,self.state_size),volatile=True)).data.numpy()
		print(q_score)
		if epsilon < np.random.sample():
			return (int)(np.argmax(q_score))
		else:
			return (int)(np.random.uniform(0,self.action_space_size))


	def train(self, env, episodes, batch_size = 100, eps_start = 0.9, eps_end = 0.05, eps_decay=2000):
		self.optimizer = torch.optim.Adam(self.q.parameters(), lr=0.001)
		self.total_steps = 0
		for episode in range(episodes):
			print('Episode [%d]'%episode)
			sys.stdout.flush()
			env.reset()
			done = False
			state = env.state
			steps = 0
			loss = 0.0
			
			while not done and steps < 2000:
				env.render()
				epsilon = eps_end + (eps_start-eps_end) * np.exp(-(self.total_steps)/eps_decay)
				action = self.take_action(state,epsilon)
				next_state, reward, done, info = env.step(action)
				if done:
					self.mem.add(Transition(torch.Tensor(state).unsqueeze(0),torch.LongTensor([action]).unsqueeze(0),
					None,torch.Tensor([reward]).unsqueeze(0)))
				else:
					self.mem.add(Transition(torch.Tensor(state).unsqueeze(0),torch.LongTensor([action]).unsqueeze(0),
					torch.Tensor(next_state).unsqueeze(0),torch.Tensor([reward]).unsqueeze(0)))
				
				state = next_state
				steps+=1
				loss += self._optimize(batch_size)
			print('Steps [%d] Loss[%.4f]'%(steps,loss/steps))

	def _optimize(self, batch_size):
		if len(self.mem)<batch_size:
			return 0
		batch = self.mem.sample(batch_size)
		batch = Transition(*zip(*batch))
		mask = []
		for state in batch.next_state:
			if state is None:
				mask.append(0)
			else:
			 mask.append(1)
		mask = torch.ByteTensor(mask)
		state_batch = Variable(torch.cat(batch.state))
		action_batch = Variable(torch.cat(batch.action))
		next_state_batch = Variable(torch.cat([state for state in batch.next_state if state is not None]),volatile = True)
		reward_batch = Variable(torch.cat(batch.reward))

		current_values = self.q(state_batch).gather(1,action_batch)
		next_values = Variable(torch.zeros(batch_size))
		next_values[mask] = self.q(next_state_batch).max(1)[0]
		next_values.volatile = False
		exp_next_values = reward_batch + self.gamma*next_values
		
		loss = F.smooth_l1_loss(current_values, exp_next_values)
		self.optimizer.zero_grad()
		loss.backward()
		self.optimizer.step()
		return loss.data.numpy()



