import gym
from torch_agent import TorchDQN
from tf_agent import TFDDQN
from tf_agent import TFDQN


def main():
	env = gym.make('CartPole-v0').unwrapped
	a = TFDDQN()
	a.train(env,300)


if __name__=='__main__':
	main()